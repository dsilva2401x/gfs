// Connect to server
var http = require('http'),
	SocketIo = require('socket.io'),
	io = require('socket.io-client');

module.exports = function (conf) { // .id .port .host

	// Attributes
		var self = this,
			socket = null,
			server = null,
			host = null,
			id = conf.id,
			port = null;

	// Methods
		var connectMaster = function (conf) { // .host .port
			conf = conf || {};
			conf.host = conf.host || 'localhost';
			conf.port = conf.port || 8080;
			socket = io.connect('http://'+conf.host+':'+conf.port, {reconnect: true});
			socket.on('connect', function() { 
				host = conf.host;
				port = conf.port;
				socket.emit('register chunkServer', {
					id: id,
					host: host,
					port: port
				});
				console.log( 'Chunkserver connected to master '+conf.host+':'+conf.port );

				// --
					/*setTimeout(function () {
						console.log('Disconnect socket');
						socket.disconnect();
						setTimeout(function () {
							console.log('Reconnect socket');
							socket.connect();
						}, 5000);
					},5000);*/
				// --
			});
		}
		self.start = function (conf) { // .masterHost .masterPort .host .port
			conf = conf || {};
			conf.port = conf.port || 8082;
			conf.host = conf.host || 'localhost';
			server = http.createServer().listen(conf.port, conf.host, function () {
				console.log( 'Chunkserver at '+conf.host+':'+conf.port );
				connectMaster({
					host: conf.masterHost,
					port: conf.masterPort
				});
			});
		}

}