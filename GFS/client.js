// Connect to server
var io = require('socket.io-client');
var express = require('express');

module.exports = function () {

	// Attributes
		var self = this,
			socket = null,
			masterUp = false;
			app = express();

	// Methods
		var setupRoutes = function () {
			app.all('/*', function (req, res, next) {
				console.log(req.method+' => '+req.url);
				next();
			});
			app.get('/file/:fileName', function (req, res){
				res.send('Respond to request file: '+req.params.fileName);
			});
			app.post('/file/:fileName', function (req, res){
				res.send('Respond to request file: '+req.params.fileName);
			});
			app.put('/file/:fileName', function (req, res){
				res.send('Respond to request file: '+req.params.fileName);
			});
			app.delete('/file/:fileName', function (req, res){
				res.send('Respond to request file: '+req.params.fileName);
			});
		}
		self.start = function (conf) { // port
			conf = conf || {};
			conf.port = conf.port || 8081;
			conf.masterPort = conf.masterPort || 8080;
			setupRoutes();
			app.listen( conf.port , function () {
				console.log( 'Client at port '+conf.port );
			});
		}
		self.connectMaster = function (conf) {
			conf = conf || {};
			conf.host = conf.host || 'localhost';
			conf.port = conf.port || 8080;
			socket = io.connect('http://'+conf.host+':'+conf.port, {reconnect: true});
			socket.on('connect', function() {
				console.log( 'Connected to master at '+conf.host+':'+conf.port );
				masterUp = true;
			});
			socket.on('disconnect', function() {
				console.log( 'Disconnected to master at '+conf.host+':'+conf.port );
				masterUp = false;
			});
		}

}