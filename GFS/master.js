var http = require('http'),
	SocketIo = require('socket.io');

module.exports = function () {
	// Attributes
		var self = this,
			chunkServers = {};

	// Methods
		self.registerChunkServer = function (chsId) {
			console.log('Chunkserver '+chsId+' registered');
			chunkServers[chsId] = {
				id: chsId,
				socket : null
			};
		}

		self.unregisterChunkServer = function (chsId) {
			console.log('Chunkserver '+chsId+' unregistered');
			chunkServers[chsId] = null;
		}

		self.start = function (conf) { // .port .host
			conf = conf || {};
			conf.port = conf.port || 8080;
			conf.host = conf.host || 'localhost';
			server = http.createServer().listen(conf.port, conf.host, function () {
				console.log( 'Master at '+conf.host+':'+conf.port );
			});
			io = SocketIo.listen(server);
			io.sockets.on('connection', function(socket) {
				var socketChunkServerId;
				socket.on('register chunkServer', function (chsData) {
					if (!chunkServers[chsData.id]) {
						console.log('Chunkserver '+chsData.id+' not registered..');
						return;
					}
					socketChunkServerId = chsData.id;
					chunkServers[chsData.id].socket = socket;
					chunkServers[chsData.id].host = chsData.host;
					chunkServers[chsData.id].port = chsData.port;
					console.log('Chunkserver '+chsData.id+' up!');

				});
				socket.on('disconnect', function() {
					if(!socketChunkServerId) return;
					chunkServers[socketChunkServerId].socket = null;
					console.log('Chunkserver '+socketChunkServerId+' down.. :(');
				});
			});
		}
}