GFS
===

## Instalación de requisitos

*Linux ubuntu*

```bash
sudo apt-get install npm
sudo apt-get install nodejs
```

## Instalación

Clonar repo

```bash
cd
mkdir GFS-so
cd GFS-so
git clone https://dsilva2401x@bitbucket.org/dsilva2401x/gfs.git .
npm install
```

## Pruebas

GFS cuenta con 3 modulos basicos: Cliente, Master, Chunkserver.
Para probar cada uno es necesario levantar una instancia de cada uno, 
hay 3 archivos de prueba que se deben abrir en 3 terminales diferentes para simular 3 servidores.

#### Levantar Master
Abrir nuevo terminal
```bash
cd master
nodejs index.js
```

#### Levantar Cliente
Abrir nuevo terminal
```bash
cd client
nodejs index.js
```
Ahora se puede entrar usando cualquier navegador a la ruta http://localhost:8081/file/archivo123


#### Levantar Chunkserver
Abrir nuevo terminal
```bash
cd chunkServer
nodejs index.js
```

