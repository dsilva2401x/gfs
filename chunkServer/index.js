
// Connect to server
var http = require('http');
var SocketIo = require('socket.io');
var io = require('socket.io-client');
var fs = require('fs');
var express = require('express');
var path = require('path');
var snappy = require('snappy');
var bodyParser = require('body-parser');
var Q = require('q');
var querystring = require('querystring');
var CONFIG = JSON.parse(fs.readFileSync('./config.json'));


var ChunkServer = module.exports = function () {

	// Attributes
		var self = this;
		var id = CONFIG.id;
		var app = express();
		var socket = null;
		var server = null;
		var host = null;
		var port = null;

	// Init
		app.use(bodyParser.json()); // for parsing application/json
		app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

	// Methods
		var connectMaster = function () {
			var conf = {};
			conf.host = CONFIG.master.host;
			conf.port = CONFIG.master.port;
			socket = io.connect('http://'+conf.host+':'+conf.port, {reconnect: true});
			socket.on('connect', function() { 
				host = conf.host;
				port = conf.port;
				socket.emit('register chunkServer', {
					id: id,
					host: CONFIG.server.host,
					port: CONFIG.server.port
				});
				console.log( 'Chunkserver connected to master at http://'+conf.host+':'+conf.port );
			});
		}

		var readChunk = function (chunkName) {
			chunkName = path.join(CONFIG.chunksDir,chunkName);
			if ( !fs.existsSync(chunkName) ) return;
			/*var fileInfo = fs.readFileSync( chunkName, 'utf-8');
			console.log( ':D :D :D' );
			console.log( fileInfo )*/
			
			// Recover info file
			var fileBuffer = '';
			var fileInfo = fs.readFileSync( chunkName, 'utf-8');
			fileInfo = JSON.parse(fileInfo);
			// Recover file
			fileInfo.chunks.forEach(function (chName) {
				var chunkBuffer = fs.readFileSync( path.join(CONFIG.chunksDir,chName), 'utf-8' );
				fileBuffer += chunkBuffer;
			});
			return {
				name: fileInfo.name,
				type: fileInfo.type,
				size: fileInfo.size,
				data: 'data:'+fileInfo.type+';base64,'+fileBuffer
			};
		}

		var writeChunks = function (chunks) {
			console.log( chunks );
			Object.keys(chunks).forEach(function (chName) {
				fs.writeFileSync(
					path.join(CONFIG.chunksDir, chName),
					chunks[chName],
					'utf-8'
				);
				console.log('Chunk '+chName+' written');
			});
		}

		var replicateInChunkServer = function (chServerData, data) {
			var deferred = Q.defer();
			var postData = querystring.stringify(data);
			var options = {
				hostname: chServerData.host,
				port: chServerData.port,
				path: '/api/chunks/',
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Content-Length': postData.length
				}
			};
			var req = http.request(options, function(res) {
				res.setEncoding('utf8');
				res.on('data', function (chunk) {
					deferred.resolve(chunk);
				});
			});
			req.on('error', function(error) {
				deferred.reject(error);
			});
			// write data to request body
			req.write(postData);
			req.end();

			return deferred.promise;
		}

		var replicateChunks = function (chServers, chunks) {
			if (!chServers || !chServers.length) return;
			var counter = 0;
			chServers.forEach(function (chServerData) {
				replicateInChunkServer(chServerData, { chunks: JSON.stringify(chunks) }).then(function (chResp) {
					console.log( 'Data written in chunk '+JSON.stringify(chServerData) );
				}).catch(function (err) {
					console.log( 'Error in chunk: '+JSON.stringify(chServerData) );
					console.log(err);
				});
			});
		}

		var setServices = function () {
			// Read chunk
			app.get('/api/chunks/:chunkId', function (req, res) {
				var chunkId = req.params.chunkId;
				var fileData = readChunk(chunkId);
				res.json(fileData);
			});

			// Write chunks
			app.post('/api/chunks/', function (req, res) {
				var chunks = req.body.chunks;
				var chServers = req.body.chServers;
				if( typeof chunks == 'string' ) chunks = JSON.parse(chunks);
				if( typeof chServers == 'string' ) chServers = JSON.parse(chServers);
				// Save chunks
				writeChunks(chunks);
				// Replicate chunks
				replicateChunks(chServers, chunks);
				res.end(':)');
			});
		}

		self.start = function () {
			conf = {};
			conf.host = CONFIG.server.host;
			conf.port = CONFIG.server.port;
			setServices();
			server = http.createServer(app).listen(conf.port, conf.host, function () {
				console.log( 'Chunkserver at http://'+conf.host+':'+conf.port );
				connectMaster();
			});
		}

}

var chs = new ChunkServer();

chs.start();