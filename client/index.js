// Connect to server
var Q = require('q');
var snappy = require('snappy');
var querystring = require('querystring');
var express = require('express');
var fs = require('fs');
var http = require('http');
var path = require('path');
var bodyParser = require('body-parser');
var CONFIG = JSON.parse( fs.readFileSync('./config.json') );

var Client = module.exports = function () {

	// Attributes
		var self = this;
		var server = null;
		var masterUp = false;
		var app = express();
		var fsTree = JSON.parse( fs.readFileSync(CONFIG.fsTreePath) );

	// Init
		app.use(bodyParser.json()); // for parsing application/json
		app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

	// Methods
		var getFileChunks = function (fileData) {

			// Generate buffer chunks
			var CHUNKSIZE = CONFIG.chunkSize;
			var ABC = CONFIG.abc;
			var CHUNKNAMESIZE = CONFIG.chunkNameSize;
			var chunks = [];
			var fileBuffer = fileData.buffer;
			var fileBuffer = fileData.buffer.replace(/^data:[a-zA-Z]+\/[a-zA-Z]+;base64,/,'');
			var bufferFlag = 0;	
			while( bufferFlag<fileBuffer.length ) {
				var chunk = fileBuffer.substring(
					bufferFlag,
					Math.min(fileBuffer.length, bufferFlag+CHUNKSIZE)
				);
				// chunk = snappy.compressSync(chunk);
				chunks.push(chunk);
				bufferFlag += CHUNKSIZE;
			}

			// Generate chunks names
			var chunksNames = [];
			for ( var i=0; i<chunks.length+1; i++ ) {
				var chunkName = '';
				for ( var j=0; j<CHUNKNAMESIZE+1; j++ ) {
					chunkName += ABC[ Math.floor( Math.random()*ABC.length ) ];
				}
				chunksNames.push( chunkName );
			}
			
			// Generate file info
			var fileInfo = {};
			fileInfo.name = fileData.name;
			fileInfo.type = fileData.type;
			fileInfo.size = fileData.size;
			fileInfo.chunks = JSON.parse(JSON.stringify(chunksNames));
			fileInfo.chunks.shift();
			chunks = [ JSON.stringify(fileInfo) ].concat( chunks );

			return {
				chunksNames : chunksNames,
				chunks : chunks
			};
		}

		var getRequest = function (options) { // options.host, options.port, options.path
			var deferred = Q.defer();
			options = {
				hostname: options.host,
				port: options.port,
				path: options.path,
				method: 'GET'
			}
			var req = http.request(options, function(res) {
				res.setEncoding('utf8');
				res.on('data', function (chunk) {
					deferred.resolve(chunk);
				});
			});
			req.on('error', function(error) {
				deferred.reject(error);
			});
			req.end();
			return deferred.promise;
		}

		var postRequest = function (options, data) { // options.host, options.port, options.path
			var deferred = Q.defer();
			var postData = querystring.stringify(data);
			options = {
				hostname: options.host,
				port: options.port,
				path: options.path,
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Content-Length': postData.length
				}
			}
			var req = http.request(options, function(res) {
				res.setEncoding('utf8');
				res.on('data', function (chunk) {
					deferred.resolve(chunk);
				});
			});
			req.on('error', function(error) {
				deferred.reject(error);
			});
			// write data to request body
			req.write(postData);
			req.end();
			return deferred.promise;
		}

		var createFolder = function (folderPath, folderName) {
			if ( !folderName ) return;
			if ( folderPath ) folderPath = folderPath.split(path.sep)
			var buffFsTree = fsTree;
			var status = true;
			if (folderPath && folderPath.length) {
				folderPath.forEach(function (cPath) {
					if (!cPath) return;
					if (!buffFsTree[cPath]) {
						status = false
						return;
					}
					buffFsTree = buffFsTree[cPath];
				});
			}
			if (buffFsTree[folderName]) return false;
			buffFsTree[folderName] = {};
			if (status) {
				fs.writeFileSync(
					CONFIG.fsTreePath,
					JSON.stringify( fsTree )
				)
			}
			return status;
		}

		var createFileinFsTree = function (folderPath, fileName) {
			if ( !fileName ) return;
			if ( folderPath ) folderPath = folderPath.split(path.sep)
			var buffFsTree = fsTree;
			var status = true;
			if (folderPath && folderPath.length) {
				folderPath.forEach(function (cPath) {
					if (!cPath) return;
					if (!buffFsTree[cPath]) {
						status = false
						return;
					}
					buffFsTree = buffFsTree[cPath];
				});
			}
			buffFsTree[fileName] = fileName;
			if (status) {
				fs.writeFileSync(
					CONFIG.fsTreePath,
					JSON.stringify( fsTree )
				)
			}
			return status;
		}

		var getChunksData = function (chunkName, chunkServers) {
			// if (!chunks || !chunks.length) return;
			var deferred = Q.defer();
			console.log( chunkName );

			var _getChunksData = function (chunkServers) {
				if (!chunkServers || !chunkServers.length) {
					deferred.reject('No ChunkServers avilable');
					return;
				}
				var cChunkServer = chunkServers.shift();
				getRequest({
					host: cChunkServer.host,
					port: cChunkServer.port,
					path: '/api/chunks/'+chunkName+'/'
				}).then(function (data) {
					if( data ) {
						deferred.resolve( data );
					}
				}).catch(function (error) {
					console.log( error );
					_getChunksData(chunkServers);
				});
			}

			_getChunksData(chunkServers);

			return deferred.promise;
		}

		var getFolderEntries = function (folderPath) {
			if (!folderPath) return;
			folderPath = folderPath.split(path.sep)
			var buffFsTree = fsTree;
			var status = true;
			folderPath.forEach(function (cPath) {
				if (!cPath) return;
				if (!buffFsTree[cPath]) {
					status = false
					return;
				}
				buffFsTree = buffFsTree[cPath];
			});
			var entries = [];
			Object.keys(buffFsTree).forEach(function (e) {
				entries.push({
					name: e,
					type: (typeof buffFsTree[e] == 'object') ? 'folder': 'file'
				});
			});
			return ( status ? entries : null );
		}

		var setupRoutes = function () {
			// Log request
			app.all('/api/*', function (req, res, next) {
				console.log(req.method+' => '+req.url);
				next();
			});

			// Create folder
			app.post('/api/folder/', function (req, res) {
				var folderPath = req.body.path;
				var name = req.body.name;
				if (!name) {
					res.end();
					return;
				}
				var status = createFolder( folderPath, name );
				res.end(status?':D':null);
			});

			// Show folder
			app.get('/api/folder/', function (req, res) {
				var folderPath = req.query.path;
				var entries = getFolderEntries(folderPath);
				res.json( entries );
			});

			app.post('/api/file/', function (req, res) {
				var filePath = req.body.path;
				var fileData = req.body;
				delete fileData['path'];
				var status = createFileinFsTree(filePath, fileData.name);
				if (!status){
					res.end();
					return;
				}
				// Get file chunks
					var fileChunks = getFileChunks(fileData);
					var chunks = {};
					fileChunks.chunksNames.forEach(function (chName, index) {
						// chunks[chName] = fileChunks.chunks[index].toString('base64');
						chunks[chName] = fileChunks.chunks[index];
					});
				// Create file in master
				postRequest({
					host: CONFIG.master.host,
					port: CONFIG.master.port,
					path: '/api/fileInfo/'
				}, {
					name: fileData.name,
					size: fileData.size,
					type: fileData.type,
					chunkName: fileChunks.chunksNames[0]
				}).then(function (chServers) {
					if (!chServers) {
						console.error('No master response data..');
						res.end();
						return;
					}
					chServers = JSON.parse(chServers);
					// Get primary Chunk Server
					var primaryChunkServer = chServers.shift();
					
					postRequest({
						host: primaryChunkServer.host,
						port: primaryChunkServer.port,
						path: '/api/chunks/'
					},{
						chunks: JSON.stringify(chunks),
						chServers: JSON.stringify(chServers)
					}).then(function (resp) {
						if (!resp) {
							res.end();
							return;
						}
						console.log( 'File '+fileData.name+' saved..' );
						res.end('Saved :D');
					}).catch(function (error) {
						console.error(error);
						res.end();
					});
				}).catch(function (error) {
					console.error(error);
					res.end();
				})
			});

			app.get('/api/file/', function (req, res) {
				var filePath = req.query.path;
				if (!filePath) {
					res.end();
					return;
				}
				filePath = filePath.split(path.sep);
				var fileName = filePath.pop();
				var buffFsTree = fsTree;
				var status = true;
				filePath.forEach(function (p) {
					if (!p) return;
					if (!buffFsTree[p]){
						status = false;
						return;
					}
					buffFsTree = buffFsTree[p];
				});
				if (!status) {
					res.end();
					return;
				}

				// Getting fsTree file name stored
				fileName = buffFsTree[fileName];

				if (!fileName) {
					res.end();
					return;
				}

				// Getting fileInfo
				getRequest({
					host: CONFIG.master.host,
					port: CONFIG.master.port,
					path: '/api/fileInfo/'+fileName
				}).then(function (fileInfo) {
					if (!fileInfo) {
						res.end();
						return;
					}
					fileInfo = JSON.parse(fileInfo);
					var chunkServers = fileInfo.chunkServers;

					if (!chunkServers.length) {
						res.end();
						return;
					}

					getChunksData(fileInfo.chunkName, fileInfo.chunkServers ).then(function (data) {
						data = JSON.parse( data );
						res.json( data );
					}).catch(function (error) {
						console.log( error );
						res.end();
					});
				}).catch(function (error) {
					console.error(error);
					res.end();
				});
			});
			
		}
		self.start = function () {
			var conf = {};
			conf.host = CONFIG.server.host;
			conf.port = CONFIG.server.port;
			setupRoutes();
			server = http.createServer(app).listen(conf.port, conf.host, function () {
				console.log( 'Client at http://'+conf.host+':'+conf.port );
			});
		}
}

var client = new Client();

client.start();