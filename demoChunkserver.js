var GFSChunkServer = require('./GFS').ChunkServer;

var chunkServer = new GFSChunkServer({
	id: process.argv[process.argv.length-1]
});

chunkServer.start();