var GFSMaster = require('./GFS').Master;

var master = new GFSMaster();

master.registerChunkServer('chs1');
master.registerChunkServer('chs2');

master.start();