var http = require('http');
var bodyParser = require('body-parser');
var fs = require('fs');
var SocketIo = require('socket.io');
var express = require('express');
var path = require('path');
var CONFIG = JSON.parse(fs.readFileSync('./config.json'));

var Master = module.exports = function () {
	// Attributes
		var self = this;
		var app = express();
		var chunkServers = {};
		var filesData = JSON.parse( fs.readFileSync(CONFIG.filesInfoPath) );

	// Init
		app.use(bodyParser.json()); // for parsing application/json
		app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

	// Methods
		var registerChunkServer = function (chsData) {
			chunkServers[chsData.id] = chsData;
			console.log('Chunkserver '+chsData.id+' registered');
		}

		var unregisterChunkServer = function (chsId) {
			delete chunkServers[chsId];
			console.log('Chunkserver '+chsId+' unregistered');
		}

		var getAvailableChunks = function () {
			var avChunkServers = [];
			var avChunkServersNames = Object.keys(chunkServers);
			avChunkServersNames = avChunkServersNames.slice(0,
				Math.min(CONFIG.chunkReplicates, avChunkServersNames.length)
			);
			avChunkServersNames.forEach(function(avChSName) {
				var chs = chunkServers[avChSName];
				avChunkServers.push({
					id: chs.id,
					host: chs.host,
					port: chs.port
				});
			});
			return avChunkServers;
		}

		var saveFileInfo = function (fileInfo) {
			filesData[fileInfo.name] = fileInfo;
			fs.writeFileSync(
				CONFIG.filesInfoPath,
				JSON.stringify(filesData)
			);
			console.log( 'File '+fileInfo.name+' saved..' );
			return fileInfo;
		}

		var deleteFileInfo = function (fileName) {
			delete filesData[fileName];
			fs.writeFileSync(
				CONFIG.filesInfoPath,
				JSON.stringify(filesData)
			);
		}

		var setServices = function () {
			// Get files name
				app.get('/api/fileInfo/', function (req, res) {
					res.json( Object.keys(filesData) );
				});
			// Read file name
				app.get('/api/fileInfo/:fileName/', function (req, res) {
					var fileName = req.params.fileName;
					res.json( filesData[fileName] );
				});
			// Delete file info
				app.delete('/api/fileInfo/:fileName', function (req, res) {
					var fileName = req.params.fileName;
					var chunkServers = filesData[fileName].chunkServers;
					deleteFileInfo(fileName);
					res.json(chunkServers);
				});
			// Write file info
				app.post('/api/fileInfo/', function (req, res) {
					var fileInfo = {
						name: req.body.name,
						type: req.body.type,
						size: req.body.size,
						chunkName: req.body.chunkName,
						chunkServers: getAvailableChunks()
					};
					if (!fileInfo.chunkServers.length) {
						res.end();
						return;
					}
					fileInfo = saveFileInfo(fileInfo);
					res.json( fileInfo.chunkServers );
				});
		}

		var setSocketEvents = function (io) {
			io.sockets.on('connection', function(socket) {
				// var socketChunkServerId;
				socket.on('register chunkServer', function (chsData) {
					socket.id = chsData.id;
					registerChunkServer({
						id: chsData.id,
						host: chsData.host,
						port: chsData.port,
						socket: socket
					});
				});
				socket.on('disconnect', function() {
					unregisterChunkServer(socket.id);
				});
			});
		}

		self.start = function () {
			var conf = {};
			conf.host = CONFIG.server.host;
			conf.port = CONFIG.server.port;
			server = http.createServer(app).listen(conf.port, conf.host, function () {
				console.log( 'Master at '+conf.host+':'+conf.port );
			});
			io = SocketIo.listen(server);
			// Set socket events
			setSocketEvents(io);
			// Set services
			setServices();
		}
}

var master = new Master();
master.start();