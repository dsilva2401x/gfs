var fs = require('fs');
const CHUNKSIZE = 10; // bytes

var splitFile = function (filePath) {
	var file = fs.readFileSync(filePath, 'utf-8');
	var chunks = [];
	var fileSize = file.length;
	var buffFlag = 0;
	while( buffFlag<fileSize ) {
		var chunk = file.substring( buffFlag,Math.min(fileSize, buffFlag+CHUNKSIZE));
		chunks.push(chunk);
		buffFlag += CHUNKSIZE;
	}
	return chunks;
}

var chs = splitFile( './arrow.png' );
var file = chs.join('');

fs.writeFileSync('cake.png', file);